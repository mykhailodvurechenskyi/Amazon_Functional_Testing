package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class CustomerPreferencesPage extends BasePage {

    @FindBy(xpath = "//span[@class='a-button-text a-declarative']")
    WebElement currencySettingsMenu;

    @FindBy(xpath = "//a[contains(text(), '€ - EUR - Euro')]")
    WebElement selectCurrency;

    @FindBy(xpath = "//input[@class='a-button-input']")
    WebElement saveChangesCurencyButton;


    public CustomerPreferencesPage(WebDriver driver){
        super(driver);
    }

    public void clickCurrencySettingsMenu(){
        currencySettingsMenu.click();
    }

    public void clickSelectCurrency(){
        selectCurrency.click();
    }

    public void clickSaveChangesCurencyButton(){
        saveChangesCurencyButton.click();
    }

    public void moveToElement(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }

}
