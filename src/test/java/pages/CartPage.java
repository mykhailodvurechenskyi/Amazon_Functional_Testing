package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CartPage extends BasePage {

    @FindBy(xpath = "//span[@id='a-autoid-0-announce']")
    WebElement quantityOfTheItemsInCart;

    @FindBy(xpath = "//div[@class='a-popover a-dropdown a-dropdown-common a-declarative']//a[contains(text(),'2')]")
    WebElement chooseQuantityOfTheItemsInCart;
    @FindBy(xpath = "")
    WebElement x;
    public void c()
    {
        x.click();
        x.getAttribute("a");
        x.getText();
        x.submit();
    }

    @FindBy(xpath = "//a[contains(text(),'0 (Delete)')]")
    WebElement deleteProductFromTheCart;

    @FindBy(xpath = "//p[@class='a-spacing-small']/span")
    List<WebElement> priceOfTheItemInTheCartList;

    @FindBy(xpath = "//div/div[@data-name='Subtotals']//span[@class='a-size-medium a-color-base sc-price sc-white-space-nowrap']")
    WebElement expectedTotalPriseOfItemsInTheCart;

    public CartPage(WebDriver driver){
        super(driver);
    }

    public void clickQuantityOfTheItemsInCart(){
        quantityOfTheItemsInCart.click();
    }

    public void clickChooseQuantityOfTheItemsInCart(){
        chooseQuantityOfTheItemsInCart.click();
    }

    public void clickDeleteProductFromTheCart(){
        deleteProductFromTheCart.click();
    }

    public List<WebElement> getPriceOfTheItemInTheCartList(){
        return priceOfTheItemInTheCartList;
    }

    public String getExpectedTotalPriseOfItemsInTheCart(){
        return expectedTotalPriseOfItemsInTheCart.getText();
    }

}
