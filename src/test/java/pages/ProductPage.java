package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {

    public ProductPage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//input[@id='add-to-cart-button']")
    WebElement addToCartButton;

    @FindBy(xpath = "//span[@id='a-autoid-0-announce']")
    WebElement quantityOfTheItems;

    @FindBy(xpath = "//a[@class='a-dropdown-link'][@id='quantity_1']")
    WebElement chooseQuantityOfTheItems;

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    WebElement searchField;

    @FindBy(xpath = "//span[@id='price_inside_buybox']")
    WebElement priceOfTheProduct;

    public void clickAddToCartButton(){
        addToCartButton.click();
    }

    public void clickQuantityOfTheItems(){
        quantityOfTheItems.click();
    }

    public void clickChooseQuantityOfTheItems(){
        chooseQuantityOfTheItems.click();
    }

    public void clearSearchField(){
        searchField.clear();
    }

    public String getPriceOfTheProduct(){
        return priceOfTheProduct.getText();
    }
}
