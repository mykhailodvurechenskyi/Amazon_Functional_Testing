package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {

    @FindBy(xpath = "//img[@class='s-image']")
    WebElement itemOnTheSearchPage;

    @FindBy(xpath = "//input[@id='low-price']")
    WebElement minPriceValueField;

    @FindBy(xpath = "//input[@id='high-price']")
    WebElement maxPriceValueField;

    @FindBy(xpath = "//input[@class='a-button-input']")
    WebElement applyPriceFilterButton;

    @FindBy(xpath = "//span[@class='a-button-text a-declarative']")
    WebElement sortByButton;

    @FindBy(xpath = "//a[@id='s-result-sort-select_2']")
    WebElement sortByPriceHighToLowOption;

    public SearchResultsPage(WebDriver driver){
        super(driver);
    }

    public void clickOnTheItemOnTheSearchPage(){
        itemOnTheSearchPage.click();
    }

    public void  fillInMinPriceValueField(String minPrice){
        minPriceValueField.sendKeys(minPrice);
    }

    public void fillInMaxPriceValue(String maxPrice){
        maxPriceValueField.sendKeys(maxPrice);
    }

    public void clickApplyPriceFilterButton(){
        applyPriceFilterButton.click();
    }

    public String getMaxPriceValue(String attribute){
        return maxPriceValueField.getAttribute(attribute);
    }

    public void clickSortByButton(){
        sortByButton.click();
    }

    public void clickSortByPriceHighToLowOption(){
        sortByPriceHighToLowOption.click();
    }
}

