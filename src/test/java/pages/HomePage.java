package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@id='twotabsearchtextbox']")
    WebElement searchingInput;

    @FindBy(xpath = "//select[@id='searchDropdownBox']")
    WebElement categoryButton;

    @FindBy(xpath = "//option[contains(text(),'Electronics')]")
    WebElement chooseCategoryButton;

    @FindBy(xpath = "//span[@class='icp-nav-link-inner']//span[@class='nav-icon nav-arrow']")
    WebElement customerPreferencesButton;

    @FindBy(xpath = "//span[@class='icp-nav-link-inner']//span[@class='nav-icon nav-arrow']")
    WebElement customerPreferencesdropDownMenu;

    @FindBy(xpath = "//div[@id='nav-flyout-icp']//span[@class='nav-text']//span")
    WebElement currentCurrency;

    @FindBy(xpath = "//span[@id='nav-cart-count']")
    WebElement actualAmountOfTheProductsInTheCart;

    @FindBy(xpath = "//a[@id='nav-cart']")
    WebElement cartButton;

    @FindBy(xpath = "//span[@class='nav-sprite nav-logo-base']")
    WebElement logoButton;

    public HomePage(WebDriver driver){
        super(driver);
    }

    public void searchingByTheKeyword(String keyword){
        searchingInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickCategoryButton(){
        categoryButton.click();
    }

    public void clickChooseCategoryButton(){
        chooseCategoryButton.click();
    }

    public void clickCustomerPreferencesButton(){
        customerPreferencesButton.click();
    }

    public WebElement getCustomerPreferencesdropDownMenu(){
        return customerPreferencesdropDownMenu;
    }

    public String getCurrentCurrency(){
        return currentCurrency.getText();
    }

    public String getActualAmountOfTheProductsInTheCart(){
        return actualAmountOfTheProductsInTheCart.getText();
    }

    public WebElement actualAmountOfTheProductsInTheCart(){
        return actualAmountOfTheProductsInTheCart;
    }

    public void clickCartButton(){
        cartButton.click();
    }

    public void clickLogoButton(){
        logoButton.click();
    }
}