package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchingTest extends BaseTests {

    private static final String SEARCHING_KEYWORD = "samsung tv";
    private static final String EXPECTED_QUERY_RESULT = "samsung+tv";
    private static final String SEARCHING_KEYWORD_FOR_THE_FILTER_PRICE_FIELD = "mouse";
    private static final String INVALID_VALUE_OF_MIN_PRICE_FIELD = "qwe";
    private static final String INVALID_VALUE_OF_MAX_PRICE_FIELD = "-10";
    private static final String SEARCHING_ATTRIBUTE = "value";
    private static final String EXPECTED_VALUE_OF_ATTRIBUTE = "";
    private static final String VALID_VALUE_OF_MAX_PRICE_FIELD_FOR_THE_FILTER = "15";

    @Test(priority = 1)
    public void checkThatUrlContainsKeyword() {
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD);
        assertTrue(getWebDriver().getCurrentUrl().contains(EXPECTED_QUERY_RESULT));
    }

    @Test(priority = 2)
    public void checkIfTheMinPriceFieldReceiveCharacters(){
        getHomePage().clickCategoryButton();
        getHomePage().clickChooseCategoryButton();
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD_FOR_THE_FILTER_PRICE_FIELD);
        getSearchResultsPage().fillInMinPriceValueField(INVALID_VALUE_OF_MIN_PRICE_FIELD);
        getSearchResultsPage().clickApplyPriceFilterButton();
        getBasePage().implicitlyWait(30);
        assertEquals(getSearchResultsPage().getMaxPriceValue(SEARCHING_ATTRIBUTE), EXPECTED_VALUE_OF_ATTRIBUTE);
    }

    @Test(priority = 3)
    public void checkIfTheMaxPriceFieldReceiveNegativeValue(){
        getHomePage().clickCategoryButton();
        getHomePage().clickChooseCategoryButton();
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD_FOR_THE_FILTER_PRICE_FIELD);
        getSearchResultsPage().fillInMaxPriceValue(INVALID_VALUE_OF_MAX_PRICE_FIELD);
        getSearchResultsPage().clickApplyPriceFilterButton();
        getBasePage().implicitlyWait(30);
        assertEquals(getSearchResultsPage().getMaxPriceValue(SEARCHING_ATTRIBUTE), EXPECTED_VALUE_OF_ATTRIBUTE);
    }

    @Test(priority = 4)
    public void checkIfTheFilterShowMaxPrice(){
        getHomePage().clickCategoryButton();
        getHomePage().clickChooseCategoryButton();
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD_FOR_THE_FILTER_PRICE_FIELD);
        getSearchResultsPage().fillInMaxPriceValue(VALID_VALUE_OF_MAX_PRICE_FIELD_FOR_THE_FILTER);
        getSearchResultsPage().clickApplyPriceFilterButton();
        getBasePage().implicitlyWait(30);
        getSearchResultsPage().clickSortByButton();
        getSearchResultsPage().clickSortByPriceHighToLowOption();
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        Double maxPriceOfTheProduct = Double.parseDouble( getProductPage().getPriceOfTheProduct().substring(1));
        assertTrue(maxPriceOfTheProduct>=Double.parseDouble(VALID_VALUE_OF_MAX_PRICE_FIELD_FOR_THE_FILTER));
    }
}
