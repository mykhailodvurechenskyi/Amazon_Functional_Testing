package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CustomerPreferencesTests extends BaseTests {

    private static final String EXPECTED_CURRENCY = "€ - EUR - Euro";

    @Test(priority = 1)
    public void changeThePaymentCurruncy(){
        getHomePage().clickCustomerPreferencesButton();
        getCustomerPreferencesPage().clickCurrencySettingsMenu();
        getCustomerPreferencesPage().clickSelectCurrency();
        getCustomerPreferencesPage().clickSaveChangesCurencyButton();
        getCustomerPreferencesPage().moveToElement(getHomePage().getCustomerPreferencesdropDownMenu());
        getBasePage().implicitlyWait(30);
        String currentPaymentCurruncy = getHomePage().getCurrentCurrency();
        assertEquals(currentPaymentCurruncy, EXPECTED_CURRENCY);
    }
}
