package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CartTests extends BaseTests {

    private static final String SEARCHING_KEYWORD = "EVERMARKET 6 Pack 8 Ounce 230ml";
    private static final String EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART = "1";
    private static final String EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART_AFTER_ORDERED_SEVERAL_POSITIONS = "2";
    private static final String EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART_AFTER_DELETING = "0";
    private static final String SEARCHING_FIRST_PRODUCT_FOR_CHECKING_TOTAL_PRICE = "pen";
    private static final String SEARCHING_SECOND_PRODUCT_FOR_CHECKING_TOTAL_PRICE = "hightligher";

    @Test(priority = 1)
    public void addItemOfTheProductToTheCart(){
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD);
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        getProductPage().clickAddToCartButton();
        getBasePage().implicitlyWait(30);
        getHomePage().clickLogoButton();
        String actualAmountOfTheProductsInTheCart = getHomePage().getActualAmountOfTheProductsInTheCart();
        assertEquals(actualAmountOfTheProductsInTheCart, EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART);
    }

    @Test(priority = 2)
    public void orderSeveralPositionOfTheSameProduct(){
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD);
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        getBasePage().implicitlyWait(30);
        getProductPage().clickQuantityOfTheItems();
        getProductPage().clickChooseQuantityOfTheItems();
        getProductPage().clickAddToCartButton();
        getBasePage().waitForElementLocated(30,getHomePage().actualAmountOfTheProductsInTheCart());
        String actualAmountOfTheProductsInTheCart = getHomePage().getActualAmountOfTheProductsInTheCart();
        assertEquals(actualAmountOfTheProductsInTheCart, EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART_AFTER_ORDERED_SEVERAL_POSITIONS);
    }

    @Test(priority = 3)
    public void changeAmountOfTheOrderProductInTheCart() {
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD);
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        getBasePage().implicitlyWait(30);
        getProductPage().clickAddToCartButton();
        getHomePage().clickCartButton();
        getCartPage().clickQuantityOfTheItemsInCart();
        getCartPage().clickChooseQuantityOfTheItemsInCart();
        getBasePage().implicitlyWait(30);
        getHomePage().clickLogoButton();
        String actualAmountOfTheProductsInTheCart = getHomePage().getActualAmountOfTheProductsInTheCart();
        assertEquals(actualAmountOfTheProductsInTheCart, EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART_AFTER_ORDERED_SEVERAL_POSITIONS);
    }

    @Test(priority = 4)
    public void deleteOrderedProductFromTheCart(){
        getHomePage().searchingByTheKeyword(SEARCHING_KEYWORD);
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        getBasePage().implicitlyWait(30);
        getProductPage().clickAddToCartButton();
        getHomePage().clickCartButton();
        getCartPage().clickQuantityOfTheItemsInCart();
        getCartPage().clickDeleteProductFromTheCart();
        getBasePage().implicitlyWait(30);
        String actualAmountOfTheProductsInTheCart = getHomePage().getActualAmountOfTheProductsInTheCart();
        assertEquals(actualAmountOfTheProductsInTheCart, EXPECTED_AMOUNT_OF_PRODUCTS_IN_THE_CART_AFTER_DELETING);
    }

    @Test(priority = 5)
    public void checkTotalPriceOfItems(){
        getHomePage().searchingByTheKeyword(SEARCHING_FIRST_PRODUCT_FOR_CHECKING_TOTAL_PRICE);
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        getBasePage().implicitlyWait(30);
        getProductPage().clickAddToCartButton();
        getProductPage().clearSearchField();
        getHomePage().searchingByTheKeyword(SEARCHING_SECOND_PRODUCT_FOR_CHECKING_TOTAL_PRICE);
        getSearchResultsPage().clickOnTheItemOnTheSearchPage();
        getBasePage().implicitlyWait(30);
        getProductPage().clickAddToCartButton();
        getHomePage().clickCartButton();
        double sumOfprices = 0;
        for(WebElement price : getCartPage().getPriceOfTheItemInTheCartList()){
            sumOfprices += Double.parseDouble(price.getText().substring(1));
        }
        int actualTotalPricesOfItemsInTheCart = (int)(sumOfprices*100);
        int expectedTotalPriseOfItemsInTheCart = (int)(Double.parseDouble( getCartPage().getExpectedTotalPriseOfItemsInTheCart().substring(1))*100);
        assertEquals(actualTotalPricesOfItemsInTheCart, expectedTotalPriseOfItemsInTheCart);
    }
}
