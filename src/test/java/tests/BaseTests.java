package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.*;

public class BaseTests {

    protected WebDriver driver;
    private static final String AMAZON_URL = "https://www.amazon.com/";

    @BeforeTest
    public void profileSetUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Gal\\IdeaProjects\\chromedriverNew\\chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(AMAZON_URL);
    }

    @AfterMethod
    public void tearDown(){
        driver.close();
    }

    public WebDriver getWebDriver(){
        return driver;
    }

    public HomePage getHomePage(){
        return new HomePage(driver);
    }

    public BasePage getBasePage(){
        return new BasePage(driver);
    }

    public CustomerPreferencesPage getCustomerPreferencesPage(){
        return new CustomerPreferencesPage(driver);
    }

    public SearchResultsPage getSearchResultsPage(){
        return new SearchResultsPage(driver);
    }

    public ProductPage getProductPage(){
        return new ProductPage(driver);
    }

    public CartPage getCartPage(){ return new CartPage(driver);}
}
